parolalar = []#parolalar adında bir liste oluşturuldu
while True:#kullanıcının istediği kadar işlem yapması için while in içinr true yazıyoruz
    print('Bir işlem seçin') #bu ve bundan sonraki 2 print te kullanıcının yaoacağı işlemleri tanıtıyoruz
    print('1- Parolaları Listele')
    print('2- Yeni Parola Kaydet')
    islem = input('Ne Yapmak İstiyorsun :')#kullanıcının yapacağı işlemi  input ile seçmesini sağlayıp bunu işleme atıyoruz
    if islem.isdigit():#burada işlemin bir sayyı olup olmadığını kontrol eddiyoruz
        islem_int = int(islem)#burada tür dönüşümü yapıyoruz ve islem_int diye bir değişkene atıyoruz.
        if islem_int not in [1, 2]:# bu if bloğunda kullanıcının bizim verdiğiğz secenekler dışında bir seçenek girmesi dshilinde
            print('Hatalı işlem girişi')# kullanıcıyı bilgilendirip kodun devam etmesini sağlıyoruz(continue ile)
            continue
        if islem_int == 2:#bu if bloğunda kullanıcının 2 numaralı işlemi seçmesi dahilinde ondan kullanıcı adı parola vb
#bilgileri girmesini isteyecek
            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')

            kullanici_adi = input('Kullanici Adi Girin :')

            parola = input('Parola :')
            parola2 = input('Parola Yeniden :')#parolayı doğru girip girmediğimizi kontrol etmek için 2. bir parola tanımlıyoruz
            eposta = input('Kayitli E-posta :')
            gizlisorucevabi = input('Gizli Soru Cevabı :')
            if kullanici_adi.strip() == '':# bu 7 if bloğunda eğer kullanıcıdan istedigi bilgiyi girmemesi dahilinde  kullanıcıyı uyarıp kodun devam etmesini sağlayacak
                print('kullanici_adi girmediz')
                continue
            if parola.strip() == '':
                print('parola girmediz')
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            if parola2 != parola:#parolanın oğrugirilip girilmediğinğ kontrol ediyoruz
                print('Parolalar eşit değil')
                continue

            yeni_girdi = {#burada bir dict oluşturulmuştur mamacımız girilen bilgleri görmek
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(yeni_girdi)#yeni_girdi dict i parolalara append ile eklenmiştir.
            continue
        elif islem_int == 1:# kullanıcı 1 işlemini girmesi dahilinde yapılacak işlemler bu bloktadır
            alt_islem_parola_no = 0#kullanııcın istediği kadar parola girmasini sağlamak için bunu tanımlayıp aşağıdaki for döngüsünü kullnıyoruz
            for parola in parolalar:#girilen parola daha önce girilmişse işlemi bir sonraki adıma taşıyo ve işlemleri bize veriyo
                alt_islem_parola_no += 1
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi')))
            alt_islem = input('Yukarıdakilerden hangisi ?: ')
            if alt_islem.isdigit():#yeniden işlemleri seçtikten sonra bir sayı olup olmadığını kontrol ediyoruz
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem):#burada tür dönüşümü yapılıyo ve parolanın doğru olup olmadığı kontrol ediliyo
                    print('Hatalı parola seçimi')
                    continue
                parola = parolalar[int(alt_islem) - 1]
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(#burada format kullanılırak sırasıyla (yukarıdaki satuı sayesinde) print edilmiştir
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue

    print('Hatalı giriş yaptınız')
